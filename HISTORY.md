# HISTORY

## 2019-11-04

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 98, add: 9 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 9 )

## 2019-10-24

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 92, add: 0 )

## 2019-10-23

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 92, add: 1 )

## 2019-10-19

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 0 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-10-18

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 0 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-10-17

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-10-05

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 90, add: 1 )

## 2019-10-03

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 89, add: 0 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 10, add: 0 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 12 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2019-10-02

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 89, add: 12 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 10, add: 1 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 12 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2019-09-26

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 9, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 89, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-09-25

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 8, add: 1 )

## 2019-09-24

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 7, add: 1 )

## 2019-09-19

### Epub

- [アプリによって管理され調教される私(被APP管理和調教的我)](h/%E3%82%A2%E3%83%97%E3%83%AA%E3%81%AB%E3%82%88%E3%81%A3%E3%81%A6%E7%AE%A1%E7%90%86%E3%81%95%E3%82%8C%E8%AA%BF%E6%95%99%E3%81%95%E3%82%8C%E3%82%8B%E7%A7%81(%E8%A2%ABAPP%E7%AE%A1%E7%90%86%E5%92%8C%E8%AA%BF%E6%95%99%E7%9A%84%E6%88%91)) - h
  <br/>( v: 1 , c: 10, add: 0 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 6, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 88, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )



