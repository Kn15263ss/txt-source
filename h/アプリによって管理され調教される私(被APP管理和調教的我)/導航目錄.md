# CONTENTS

アプリによって管理され調教される私(被APP管理和調教的我)  
アプリによって管理され調教される私  
被APP管理和調教的我  

作者： 水無夕  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E3%82%A2%E3%83%97%E3%83%AA%E3%81%AB%E3%82%88%E3%81%A3%E3%81%A6%E7%AE%A1%E7%90%86%E3%81%95%E3%82%8C%E8%AA%BF%E6%95%99%E3%81%95%E3%82%8C%E3%82%8B%E7%A7%81(%E8%A2%ABAPP%E7%AE%A1%E7%90%86%E5%92%8C%E8%AA%BF%E6%95%99%E7%9A%84%E6%88%91).ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/h/%E8%A2%ABAPP%E7%AE%A1%E7%90%86%E5%92%8C%E8%AA%BF%E6%95%99%E7%9A%84%E6%88%91.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/h/out/%E8%A2%ABAPP%E7%AE%A1%E7%90%86%E5%92%8C%E8%AA%BF%E6%95%99%E7%9A%84%E6%88%91.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/h/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/h/アプリによって管理され調教される私(被APP管理和調教的我)/導航目錄.md "導航目錄")




## [序章 - 當我還不是奴隸時的故事](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B)

- [看到APP廣告的那一天](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0010_%E7%9C%8B%E5%88%B0APP%E5%BB%A3%E5%91%8A%E7%9A%84%E9%82%A3%E4%B8%80%E5%A4%A9.txt)
- [註冊後拿到錢跟朋友一起玩出去玩很開心](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0020_%E8%A8%BB%E5%86%8A%E5%BE%8C%E6%8B%BF%E5%88%B0%E9%8C%A2%E8%B7%9F%E6%9C%8B%E5%8F%8B%E4%B8%80%E8%B5%B7%E7%8E%A9%E5%87%BA%E5%8E%BB%E7%8E%A9%E5%BE%88%E9%96%8B%E5%BF%83.txt)
- [與視頻連動被虐愛的高級會員註冊](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0030_%E8%88%87%E8%A6%96%E9%A0%BB%E9%80%A3%E5%8B%95%E8%A2%AB%E8%99%90%E6%84%9B%E7%9A%84%E9%AB%98%E7%B4%9A%E6%9C%83%E5%93%A1%E8%A8%BB%E5%86%8A.txt)
- [虐愛器具的選擇](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0040_%E8%99%90%E6%84%9B%E5%99%A8%E5%85%B7%E7%9A%84%E9%81%B8%E6%93%87.txt)
- [因為選了貞操帶_所以要脫毛跟測量的我](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0050_%E5%9B%A0%E7%82%BA%E9%81%B8%E4%BA%86%E8%B2%9E%E6%93%8D%E5%B8%B6_%E6%89%80%E4%BB%A5%E8%A6%81%E8%84%AB%E6%AF%9B%E8%B7%9F%E6%B8%AC%E9%87%8F%E7%9A%84%E6%88%91.txt)
- [第一次剃陰毛](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0060_%E7%AC%AC%E4%B8%80%E6%AC%A1%E5%89%83%E9%99%B0%E6%AF%9B.txt)
- [跟朋友比起來更優先選了穿戴貞操帶的一天](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0070_%E8%B7%9F%E6%9C%8B%E5%8F%8B%E6%AF%94%E8%B5%B7%E4%BE%86%E6%9B%B4%E5%84%AA%E5%85%88%E9%81%B8%E4%BA%86%E7%A9%BF%E6%88%B4%E8%B2%9E%E6%93%8D%E5%B8%B6%E7%9A%84%E4%B8%80%E5%A4%A9.txt)
- [第一次穿戴貞操帶](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0080_%E7%AC%AC%E4%B8%80%E6%AC%A1%E7%A9%BF%E6%88%B4%E8%B2%9E%E6%93%8D%E5%B8%B6.txt)
- [貞操帶的貼合感](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0090_%E8%B2%9E%E6%93%8D%E5%B8%B6%E7%9A%84%E8%B2%BC%E5%90%88%E6%84%9F.txt)
- [穿著貞操帶上廁所](00010_%E5%BA%8F%E7%AB%A0%20-%20%E7%95%B6%E6%88%91%E9%82%84%E4%B8%8D%E6%98%AF%E5%A5%B4%E9%9A%B8%E6%99%82%E7%9A%84%E6%95%85%E4%BA%8B/0100_%E7%A9%BF%E8%91%97%E8%B2%9E%E6%93%8D%E5%B8%B6%E4%B8%8A%E5%BB%81%E6%89%80.txt)

