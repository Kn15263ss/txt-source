﻿
「唔嗯唔嗯。」
「啊啦啊啦。」

在父親他們眼前，堂堂正正的和莎莉亞抱著的我，感到臉都變紅了。
糟糕了啊！完全忘記父親他們的存在了！
因、因為就算時間很短，和莎莉亞她們分開，我也感到寂寞了嗎？
也不知道在跟誰解釋，心裡著急的往父親他們那邊看過去，兩個人都用著"不用說，我懂"的溫暖眼神看著我。
不要啊啊啊啊啊啊啊啊！好害羞！糟糕，這個超讓人害羞的啊！？
真虧這個世上的現充能夠忍受這麼讓人害羞的事啊！？這對我還說太早了！
在我想著這些無聊的事情時，莎莉亞注意到父親他們的存在了。

「啊勒？那些人是？」
「嘿！？啊、啊啊。......這些人是，和我有因緣的人們......的樣子？」
「為什麼是疑問句？」

不，因為因緣的基準我也不是說很明白......。
然後，賽亞諾斯用溫柔的表情像莎莉亞搭話了。

「好久不見了呢，猩猩小姐......不對，莎莉亞小姐。」
「咦？......啊！賽亞諾斯先生！？」

為什麼賽亞諾斯看到人類型態的莎莉亞，能察覺到是那隻猩猩啊！？
莎莉亞變成人類時，應該確實是在打倒賽亞諾斯之後才對......。
而且，莎莉亞也馬上就認出賽亞諾斯了......是認不出來的我比較奇怪嗎？

「咦，不過記得賽亞諾斯先生......。」
「唔嗯。我在和誠一先生的勝負中落敗，從這個世上完全消滅了......不過，因為某些因果，我在和誠一先生在冥界相遇了，也和我重要的......瑪莉相會了。」
「這麼說，妳就是瑪莉小姐嗎？」

莎莉亞向站在賽亞諾斯旁的瑪莉說著，瑪莉小姐恭敬的行了個禮。

「是的......我就是侍奉賽亞諾斯大人的女僕瑪莉。」

聽了瑪莉小姐的自我介紹後，莎莉亞浮現滿臉的笑容，抱住了瑪莉小姐。

「太好了！和賽亞諾斯先生在一起了呢！」
「誒！？那、那個......是的。」

瑪莉小姐也是，雖然一開始對莎莉亞的行動張大了眼睛，在害羞的看向賽亞諾斯後，紅著臉的點了點頭。
......因為我和莎莉亞讀過了賽亞諾斯的人生，見到了瑪莉小姐和賽亞諾斯經過的結局。
對這兩個人能像這樣幸福的在一起的事，莎莉亞很開心吧。
對保持著抱著瑪莉小姐狀態的莎莉亞，這次是魯希烏斯先生說話了。

「呀。知道我的事嗎？哎呀，不可能知────。」
「初代魔王大人？」
「為什麼會知道！？」

連問的本人魯希烏斯先生都因為莎莉亞的話震驚了。不，雖說我也被嚇到了就是。真的為啥會知道？

「那麼，知道我的們事情嗎？」
「等等，再怎麼說那也不可能知道的吧？」

阿貝爾用笑笑的臉向莎莉亞這麼問著，對此夥伴的安娜則是給了他一個白眼。
不過，莎莉亞用笑臉斷言了。

「勇者阿貝爾先生和戰士的賈爾斯先生，然後是獵人的安娜小姐跟賢者的莉莉亞娜小姐！」
「騙人的吧！？」
「真的說對了......。」

在阿貝爾的震驚後，賈爾斯小聲的唸著。

「那、那個......為什麼對我們的事情......？」

莉莉亞娜不由自主的這麼問著，莎莉亞在做出稍微思考了一下的動作後，開口了。

「野生的直覺！」

野生的直覺是什麼東西啊。
莎莉亞偶爾會靠這個野生的直覺來行動，一次也沒有失誤過。已經到預知的等級了吧？野生還真不妙啊。
聽到莎莉亞的回答，阿貝爾他們果然露出了微妙的表情。
......嘛，因為莎莉亞為了學習說話而讀的，就是阿貝爾的日記，所以知道名字和猜中的事情也......不對，就算知道名字也猜不中吧。
雖說使用『鑑定』的話就另當別論了。
話說回來，雖然是事到如今，不管是我還是莎莉亞都讀過了阿貝爾的日記了......本來，日記除了交換日記之類的不在限制，不是會想讓人看見的東西吧。
然後，那被我們看到了......嗯，別說了吧。哎呀，雖說沒有寫上什麼奇怪的事情，心情上果然還是會不高興吧。

「......我，還記得？」
「寶箱先生！還記得呦！」

一個人在為阿貝爾的日記思考著時，莎莉亞在和寶箱說著話。
......哎呀，嗯。與寶箱的相遇......和別離，我的精神受創了。雖然已經說過好幾次了，我沒有打倒的打算啊！只是我的身體比預料的更加不妙而已啊！
結果莎莉亞雖然對大部分的人都是一次就說中，看到娜秋莉安納小姐時，歪了歪頭。

「呃（えっと）......我的名字是娜秋莉安納。那個......妳知道關於我的事情嗎？」
「唔～嗯......不知道娜秋莉安納小姐的事呢......。」
「是這樣啊......。」

娜秋莉安納小姐浮現出微妙的表情。
那也是當然的吧。
要說為什麼，明明是因為我的影響才被叫來的，對那因緣卻完全沒頭緒啊。
真的是，在哪裡產生的因緣啊......。
在我思考著與娜秋莉安納小姐的因緣時，父親他們跟莎莉亞搭話了。

「妳好，莎莉亞小姐......可以這樣稱呼妳嗎？」
「咦？」
「看來誠一受到妳的關照了呢。」
「真的很謝謝妳呢，雖然只有看到一會兒，妳很重視著誠一......而且，誠一也很重視著妳這件事已經很讓人明白了。」

雖然父親跟母親說得沒錯，但被人這樣說讓人格外害羞啊！
莎莉亞看到父親他們，一瞬間露出呆滯的表情後，眼神閃閃發亮著。

「該不會......是誠一的父親和母親嗎！？」
「沒錯喔。」

父親跟母親露出溫柔的微笑回答了，莎莉亞慌張的轉向父親他們。

「那個......我叫做莎莉亞！是誠一的......新娘子！」

對滿臉通紅的莎莉亞，賽亞諾斯他們不知道為什麼點了點頭，阿貝爾他們則是笑笑的看著我們。
然後，父親他們是────。

「誠先生，聽到了嗎！？」
「啊啊，確實聽到了呢。」
「這麼可愛的孩子不只是誠一的女朋友還是新娘子什麼的......今天要吃紅豆飯呢！」
「真的是在爸爸我們不在的期間裡，成長得很出色了啊。」
「不要再說了！」

不對，不是在否定莎莉亞說的新娘子的事喔！我也是最喜歡了！
不過，被自己的雙親說出這件事沒想到相擁的場面被看到會比想像中要來的羞恥啊！
雖然比起父親他們，周圍的視線更讓人鬱悶就是！
在我因為羞恥而苦惱時，感覺到了朝這個鬥技場跑來的人的氣息。
朝那個方向投去視線────。

「喂，莎莉亞！突然跑出去是────誠一！？」
「啊，主人！」
「誠一大哥哥！？」

阿爾他們看到我十分吃驚。
雖然因為羞恥讓腦袋轉不過來，不過只有莎莉亞在這裡確實很奇怪。
話說，為什麼會知道我們回到這個地方了？
在我吃驚和想著那些事情時，阿爾她們朝我跑了過來，然後就這樣抱住了我。

「誠一......！沒事真的太好了......！」
「主人，我可是相信著的喔！肯定會讓冥界屈服後回來的！」
「......誠一大哥哥，歡迎回來。」

就算是因為接二連三發生的事而手忙腳亂的我，也緊緊的回抱著阿爾她們。

「啊啊，我回來了。」

被人擔心到這種程度什麼的......真的對我來說太浪費了啊。
只是，露璐奈到底是怎麼看我的啊。雖然就結果來說是類似的狀況沒錯，所以沒辦法反駁就是！
莎莉亞也是，露出溫柔的表情，看著我和阿爾她們相擁的樣子。
可是────。

「誠、誠先生。在我們不知道的期間，誠一真的成長了呢......。」
「啊、啊啊......預料之外呢......。」

父親他們看見我和阿爾她們的樣子，擺出了一副苦瓜臉。
......又忘記父親他們了。

◆◇◆

────溫布格王國的王都泰爾貝爾。

「唔─姆......。」

在王城的一間房間裡，溫布格王國的國王藍傑在某份書信前呻吟著。

「那麼，該怎麼辦才好呢......。」
「藍傑殿下！聽說有事要找我，本人加斯魯，以兔子跳參上了！」
「有那個情報嗎？」

在那個藍傑的房間裡，連門都沒敲就出現的是，像平常一樣裸著上半身穿著三角泳褲的加斯魯。
本來的話，對王族的防兼連門都沒敲就闖入是極為不敬的，藍傑也嘆了口氣。

「抱歉，突然把你叫出來啊。」
「不用在意喔！因為就算我不在，公會還是能夠照常運作啊！」
「那你對公會不就是不需要的嘛。」
「說話好毒！」

明明被說了相當過份的話，加斯魯的表情也沒有特別傷心，只是拍著頭開朗的笑著。

「嘛，算了。那麼，有什麼事情嗎？」
「啊啊......從國家對公會，發出一個委託。」
「......喔？」

一聽到從國家發出的委託的話，加斯魯也變成認真的氛圍了。

「那是想要『高效率練出肌肉的方法』的講義嗎？」
「沒可能是那樣的吧！？你連腦袋都是肌肉嗎！」
「哎呀，就算你這麼說。」
「沒在誇獎你！」

認真的氛圍只是錯覺而已。

「不是要講義的話，到底還能委託公會什麼？」
「不對，有很多的吧！？就算不說這次的案件，還有魔物討伐之類的啊！」
「啊─。還有那種事呢。」
「誰來把這傢伙從公會長換掉啊！」

藍傑的吐槽是理所當然的。

「那麼，到底是什麼？參加戰爭之類的內容的話，可是要斷然拒絕的啊......。」
「別說那種話啊......希望能夠招集所有的S級冒險者。」
「什麼！？」

加斯魯吃驚的用認真的表情詢問著。

「那指的是只有實力是S級的冒險者嗎？還是說，實力和稱號都是S級的冒險者呢？」
「當然是指實力和稱號都是S級的傢伙啊！？為啥我得要僱用你那邊只有實力是S級的變態不可啊！」
「原來如此......不過，真正的S級冒險者們都是隨著各自的慾望來行動的啊。」
「糟糕。對委託公會只有滿滿的不安啊。」
「嘛，請安心吧！至少實力是不假的！不過......為什麼想要招集S級的冒險者呢？」

加斯魯說出了理所當然的疑問，藍傑將剛才看的書信遞給加斯魯看並說了。

「從魔王那，送來了會議的邀請。」

