──這不是什麼值得主動提起的事。

從小我的父母就告訴我，到十八歲為止都不能接近除了家人以外的男性，只要和他們保持最低限度的交流就足夠了。直到十五歲的現在，我都一直遵守著這項規定。

來到迷宮國之後，我仍然和家人──也就是我的父母和哥哥一起行動。

我從小就不擅長和無論做什麼都比我優秀的哥哥相處。我總是不知不覺中服從哥哥所說的話，這讓父母誤以為我們是一對感情融洽的兄妹，還為此感到高興，所以我無法坦白自己其實是怎麼看待哥哥的。

我不擅長和男人打交道，因為他們一定會像哥哥一樣，認為所有事情都會順著自己的意，並為了一己之私而試圖利用周圍的人。

───

我原本是這麼認為的，但不知從何時開始，唯有在面對『他』時，男人對我而言不再是種棘手的存在。

在蒸氣彌漫的浴室中，我和鏡花及特蕾吉亞正一起為有人沖洗身體。有人似乎很難為情的樣子，鏡花則是一副心神不定的表情。我沖洗著他的手臂，同時佩服他的肌肉竟然意外地結實──

「⋯⋯我怎麼會做這種夢呢？」

我突然在被窩中醒來。時間還是半夜，我像個孩子似地縮起身子，以這樣的姿勢將自己裹在棉被裡。

和我同房、直到剛才還睡到發出打呼聲的美咲不見了。我在睡前和她聊了很多，那時的話題也是圍繞著『他』打轉。

後部・有人。他是我們隊上的領導者，也是所有人之中年紀最大的男性。

腦海中浮現出他的臉，讓我不知怎地無法繼續躺著，起身坐在床角。這個房間的床墊很厚，床腳也很高，就算坐著，腳還是碰不到地面，就差了那麼一點。這絕非意味著我的身型很嬌小，而是這張床太大了。

但就算編這種藉口，我也才十五歲，無法否認從有人的角度來看，自己就是個孩子──

在鏡花表示擔心特蕾吉亞單獨和有人一起洗澡，所以決定和他們一起洗時，我想用某種形式表達對有人的感謝，於是打算為他刷背。我在幫他沖洗時原本什麼也沒多想，但鏡花對此感到相當難為情，讓我一開始還覺得「既然會這麼害羞，那又何必逞強呢」

然而因為有人說出的短短幾個字，讓我頓時理解了鏡花的心情。

「⋯⋯他向我道謝了。」

沒想到自己甚至會因為這種事而做夢。

我並不是為了得到感謝才這麼做的。我只不過是想回報有人而已。

他打從一開始就總是一心想著幫助別人。在我入隊之後也老是受到他的幫助，只要有他站在身後，我就會感到相當放心。即使是在自己的意識被劍所支配、差點就要失去自我時，有人的聲音也會及時將我拉回來。總是相當努力且正直的他，會出聲呼喚我的名字。

「⋯⋯不對，我抱持的並不是那種心意。只不過是尊敬他罷了。」

我搖了搖頭。即使對他再怎麼難以啟齒，但只要化作語言說出口，自己的心意就會變得明確。

我認為尊敬他的自己是很值得驕傲的。雖然他原本是以鏡花部下的身分工作，所以在她面前抬不起頭來，但自從來到迷宮國之後，鏡花也相當尊敬有人，並將他當成隊長看待。

有時看到有人和鏡花在交談，會讓我感受到大人間的互動是如此自然，要是他沒有來到迷宮國，我和他是絕對不會有交集的。

──我現在還沒有資格說出「能轉生真是太好了」這種話。

但能認識珠洲菜、加入有人的隊伍，並和大家在一起，對我而言是相當重要的事，今後我也絕不希望失去目前的關係。所以，我不會再受到劍的操弄了。無論詛咒之力有多強大，我都不會屈服。

就在我如此下定決心時，美咲打開門探出頭來。

「啊，太好了～原來艾莉小姐醒著啊～我剛去小圓和梅麗莎的房間串了一下門子，艾莉小姐要不要一起來場深夜的午茶時光呢？」
「嗯，我知道了。如果她們不介意我過去打擾的話。」
「哇啊⋯⋯艾莉小姐光是說出『嗯』這個字感覺就好可愛。你平時總是會老成地回答『好的』對吧。」
「美咲你才是，因為我的年紀比你還小，所以你隨時都可以展現老成的一面喔。」
「哇！也就是說我裝出一副姊姊的樣子也沒關係嗎！那我明天可不可以幫你系緞帶？」
「如果你能綁得漂亮的話，拜託你也是可以啦。」

或許美咲、珠洲菜和其餘所有人總有一天，也將體會到和我一樣的心情吧。

雖然不曉得我是否會坦率地將這份心情告訴有人就是了。